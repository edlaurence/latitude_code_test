from src.file_generator import FileOperator

# Instantiate file operator class with desired file spec
file_operator = FileOperator(file_spec_path="spec.json")

# Generate fixed width file with 100 rows of data
file_operator.generate_file("gen_file_output.txt", 100)

# Parse data back
data = file_operator.parse_file(target_file_path="gen_file_output.txt")

# Write parsed data to CSV
file_operator.parse_data_to_csv('csv_file.csv', data)