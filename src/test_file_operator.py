from unittest import TestCase
from src.file_generator import FileOperator, InvalidSpecMismatch, InvalidSpecBadEncoding, InvalidSpecBadFilePath, InvalidParseFilePath, InvalidCsvData
import json
import os
import pandas as pd

TEST_FILE_SPEC_PATH = "test_file_spec.json"
TEST_FILE_OUTPUT_TXT_PATH = "test_file_output.txt"
TEST_CSV_FILE_PATH = "csv_file.csv"

class TestFileOperator(TestCase):

    def tearDown(self):

        # Remove any temp test files if they exist
        for rem_file_path in [TEST_FILE_SPEC_PATH, TEST_FILE_OUTPUT_TXT_PATH, TEST_CSV_FILE_PATH]:

            if os.path.exists(rem_file_path):
                os.remove(rem_file_path)

    def test_file_operator_instantiate_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "True",
            "DelimitedEncoding": "utf-8"
        }

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_op = FileOperator(TEST_FILE_SPEC_PATH)

        # Check specs in file operator are correct
        self.assertListEqual(file_op._column_names, ['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10'])
        self.assertListEqual(file_op._offsets, ['5', '12', '3', '2', '13', '7', '10', '13', '20', '13'])
        self.assertEqual(file_op._fixed_width_encoding, 'cp1252')
        self.assertEqual(file_op._include_header, True)
        self.assertEqual(file_op._delimited_encoding, 'utf_8')

    def test_file_operator_instantiate_fail_mismatch(self):

        file_spec_json = {
                            "ColumnNames": [
                                "f1",
                                "f2",
                                "f3",
                                "f4",
                                "f5",
                                "f6",
                                "f7",
                                "f8",
                                "f9",
                                "f10"
                            ],
                            "Offsets": [
                                "5",
                                "12",
                                "3",
                                "2",
                                "13",
                                "7"
                            ],
                            "FixedWidthEncoding": "windows-1252",
                            "IncludeHeader": "True",
                            "DelimitedEncoding": "utf-8"
                        }

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        with self.assertRaises(InvalidSpecMismatch) as e:
            file_operator = FileOperator(TEST_FILE_SPEC_PATH)

    def test_file_operator_instantiate_fail_bad_encoding(self):

        file_spec_json = {
                            "ColumnNames": [
                                "f1",
                                "f2",
                                "f3",
                                "f4",
                                "f5",
                                "f6",
                                "f7",
                                "f8",
                                "f9",
                                "f10"
                            ],
                            "Offsets": [
                                "5",
                                "12",
                                "3",
                                "2",
                                "13",
                                "7",
                                "10",
                                "13",
                                "20",
                                "13"
                            ],
                            "FixedWidthEncoding": "windows-blah-blah",
                            "IncludeHeader": "True",
                            "DelimitedEncoding": "utf-8"
                        }

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        with self.assertRaises(InvalidSpecBadEncoding) as e:
            file_operator = FileOperator(TEST_FILE_SPEC_PATH)

    def test_file_operator_instantiate_fail_bad_file_spec_path(self):

        with self.assertRaises(InvalidSpecBadFilePath) as e:
            file_operator = FileOperator("this_is_a_bad_file_spec_path")

    def test_file_operator_generate_include_header_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "True",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Check output file created
        self.assertTrue(os.path.exists(TEST_FILE_OUTPUT_TXT_PATH))

        # Read output file with pandas
        widths = [int(width) for width in file_spec_json['Offsets']]
        df = pd.read_fwf(TEST_FILE_OUTPUT_TXT_PATH, widths=widths)

        # Check data dims are correct
        self.assertEqual(df.shape, (num_data_rows, len(file_spec_json['ColumnNames'])))

        # Check header row is correct
        self.assertListEqual(df.columns.to_list(), file_spec_json['ColumnNames'])

    def test_file_operator_generate_include_header_diff_columns_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "True",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Check output file created
        self.assertTrue(os.path.exists(TEST_FILE_OUTPUT_TXT_PATH))

        # Read output file with pandas
        widths = [int(width) for width in file_spec_json['Offsets']]
        df = pd.read_fwf(TEST_FILE_OUTPUT_TXT_PATH, widths=widths)

        # Check data dims are correct
        self.assertEqual(df.shape, (num_data_rows, len(file_spec_json['ColumnNames'])))

        # Check header row is correct
        self.assertListEqual(df.columns.to_list(), file_spec_json['ColumnNames'])

    def test_file_operator_generate_no_header_success(self):
        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "False",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Check output file created
        self.assertTrue(os.path.exists(TEST_FILE_OUTPUT_TXT_PATH))

        # Read output file with pandas
        widths = [int(width) for width in file_spec_json['Offsets']]
        df = pd.read_fwf(TEST_FILE_OUTPUT_TXT_PATH, widths=widths, header=None)

        # Check data dims are correct
        self.assertEqual(df.shape, (num_data_rows, len(file_spec_json['ColumnNames'])))

    def test_file_operator_parse_header_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "True",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Parse generated file
        data = file_operator.parse_file(TEST_FILE_OUTPUT_TXT_PATH)

        # Check header
        self.assertListEqual(data[0], file_spec_json['ColumnNames'])

        # Check data body number of rows
        self.assertEqual(len(data), num_data_rows + 1)

        # Check data body row size
        for row_num in range(0, num_data_rows):
            self.assertEqual(len(data[row_num]), len(file_spec_json['ColumnNames']))

    def test_file_operator_parse_no_header_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "False",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Parse generated file
        data = file_operator.parse_file(TEST_FILE_OUTPUT_TXT_PATH)

        # Check header
        self.assertNotEqual(data[0], file_spec_json['ColumnNames'])

        # Check data body number of rows
        self.assertEqual(len(data), num_data_rows)

        # Check data body row size
        for row_num in range(0, num_data_rows):
            self.assertEqual(len(data[row_num]), len(file_spec_json['ColumnNames']))

    def test_file_operator_parse_fail_bad_file_path(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "False",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        with self.assertRaises(InvalidParseFilePath) as e:
            file_operator.parse_file("this_is_a_bad_parse_file_path")

    def test_file_operator_parse_to_csv_header_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "True",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Parse generated file
        data = file_operator.parse_file(TEST_FILE_OUTPUT_TXT_PATH)

        # Write parsed data to CSV
        file_operator.parse_data_to_csv(TEST_CSV_FILE_PATH, data)

        df = pd.read_csv(TEST_CSV_FILE_PATH)

        # Check data dims are correct
        self.assertEqual(df.shape, (num_data_rows, len(file_spec_json['ColumnNames'])))

        # Check header row is correct
        self.assertListEqual(df.columns.to_list(), file_spec_json['ColumnNames'])

    def test_file_operator_parse_to_csv_no_header_success(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "False",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        # Generate file
        file_operator.generate_file(TEST_FILE_OUTPUT_TXT_PATH, num_data_rows)

        # Parse generated file
        data = file_operator.parse_file(TEST_FILE_OUTPUT_TXT_PATH)

        # Write parsed data to CSV
        file_operator.parse_data_to_csv(TEST_CSV_FILE_PATH, data)

        # Read parsed csv to dataframe
        df = pd.read_csv(TEST_CSV_FILE_PATH, header=None)

        # Check data dims are correct
        self.assertEqual(df.shape, (num_data_rows, len(file_spec_json['ColumnNames'])))

    def test_file_operator_parse_to_csv_fail_bad_data(self):

        file_spec_json = {
            "ColumnNames": [
                "f1",
                "f2",
                "f3",
                "f4",
                "f5",
                "f6",
                "f7",
                "f8",
                "f9",
                "f10"
            ],
            "Offsets": [
                "5",
                "12",
                "3",
                "2",
                "13",
                "7",
                "10",
                "13",
                "20",
                "13"
            ],
            "FixedWidthEncoding": "windows-1252",
            "IncludeHeader": "False",
            "DelimitedEncoding": "utf-8"
        }

        num_data_rows = 100

        with open(TEST_FILE_SPEC_PATH, "w+") as test_file_spec:
            json.dump(file_spec_json, test_file_spec)

        # Instantiate file operator
        file_operator = FileOperator(TEST_FILE_SPEC_PATH)

        bad_data = [
                    ['This is some bad data which should all be strings', 123, False],
                    ['This is some bad data which should all be strings', 123, False],
                    ['This is some bad data which should all be strings', 123, False]
        ]

        with self.assertRaises(InvalidCsvData) as e:
            file_operator.parse_data_to_csv(TEST_CSV_FILE_PATH, bad_data)



