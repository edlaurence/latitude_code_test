import json, random, struct, os
from encodings.aliases import aliases


class InvalidSpecMismatch(Exception):
    def __init__(self):
        super().__init__("The dimensions of `ColumnNames` and `Offsets` are not equal")
        return


class InvalidSpecBadEncoding(Exception):
    def __init__(self, encoding_name="unknown"):
        self.encoding_name = encoding_name
        super().__init__("`{}` is not a valid encoding or encoding alias".format(encoding_name))
        return


class InvalidSpecBadFilePath(Exception):
    def __init__(self, spec_file_path):
        self.spec_file_path = spec_file_path
        super().__init__("The JSON file at `{}` could not be found".format(spec_file_path))
        return


class InvalidParseFilePath(Exception):
    def __init__(self, target_file_path):
        self.target_file_path = target_file_path
        super().__init__("The TXT file at `{}` could not be found".format(target_file_path))
        return


class InvalidCsvData(Exception):
    def __init__(self):
        super().__init__("The data provided for CSV writing is not `str` type")
        return


class FileOperator:

    _file_spec_path = None
    _column_names = None
    _offsets = None
    _fixed_width_encoding = None
    _delimited_encoding = None
    _include_header = None

    def __init__(self, file_spec_path):

        if not os.path.exists(file_spec_path):
            raise InvalidSpecBadFilePath(file_spec_path)

        self.file_spec_path = file_spec_path

        return

    def generate_file(self, output_file_path, num_rows=10):

        with open(output_file_path, "w+", encoding=self._fixed_width_encoding) as output_file:

            # Write header if requested in spec
            if self._include_header:
                self.__write_fixed_width_line(output_file, self._column_names)

            for row_num in range(0, num_rows):

                # Generate random int values for the row to populate columns
                col_vals = random.sample(range(0, 30), len(self._column_names))

                # Write row to file
                self.__write_fixed_width_line(output_file, col_vals)

        return

    def parse_file(self, target_file_path=None):

        # Check valid target file path
        if not os.path.exists(target_file_path):
            raise InvalidParseFilePath(target_file_path)

        # Assemble parser for file spec
        frmt_string = ' '.join('{}{}'.format(abs(int(offset)), 'x' if int(offset) < 0 else 's') for offset in self._offsets)
        field_struct = struct.Struct(frmt_string)
        parser = field_struct.unpack_from

        # Read fixed-width file
        with open(target_file_path, "r+", encoding=self._fixed_width_encoding) as target_file:

            data = []
            for line in target_file:

                # Parse line against widths
                raw_line_data = parser(line.encode())

                # Extract and format line data
                line_data = self.__get_line_data_from_raw(raw_line_data)

                # Append to file data
                data.append(line_data)

        return data

    def parse_data_to_csv(self, csv_file_path, data):

        # Check all data are str type
        if not all(isinstance(val, str) for val in [item for sublist in data for item in sublist]):
            raise InvalidCsvData

        # Create CSV and write data
        with open(csv_file_path, "w+", encoding=self._delimited_encoding) as csv_file:

            for row in data:
                csv_file.write(','.join(row) + '\n')

    def __write_fixed_width_line(self, output_file, line_data):

        output_file.write(
            ''.join([str(col_val).ljust(int(offset)) for col_val, offset in zip(line_data, self._offsets)]) + '\n')

    @staticmethod
    def __get_line_data_from_raw(raw_line_data):

        return [raw_value.decode().strip() for raw_value in raw_line_data]  # if the file spec included type we'd cast here

    @staticmethod
    def __find_encoding(encoding_name):

        # Replace hyphens with underscores
        encoding_name_frmt = encoding_name.replace('-', '_')

        # Get encoding matches in aliases
        encoding_matches = [(k, v) for k, v in aliases.items() if encoding_name_frmt in k or encoding_name_frmt in v]

        # Retrieve encoding value or alias key
        if encoding_matches:
            encoding = encoding_matches[0][1]
        else:
            encoding = None

        return encoding

    """Getters"""
    @property
    def file_spec_path(self):
        return self._file_spec_path

    """Setters"""
    @file_spec_path.setter
    def file_spec_path(self, file_spec_path):

        self._file_spec_path = file_spec_path

        # Get file specs
        with open(file_spec_path, "r+") as json_spec_file:
            self._file_spec = json.load(json_spec_file)

        # Assign specs
        self._column_names = self._file_spec.get('ColumnNames')
        self._offsets = self._file_spec.get('Offsets')
        self._include_header = self._file_spec.get('IncludeHeader').lower() == "true"
        self._fixed_width_encoding = FileOperator.__find_encoding(self._file_spec.get('FixedWidthEncoding'))
        self._delimited_encoding = FileOperator.__find_encoding(self._file_spec.get('DelimitedEncoding'))

        if len(self._column_names) != len(self._offsets):
            raise InvalidSpecMismatch

        if not self._fixed_width_encoding:
            raise InvalidSpecBadEncoding(self._file_spec.get('FixedWidthEncoding'))

        if not self._delimited_encoding:
            raise InvalidSpecBadEncoding(self._file_spec.get('DelimitedEncoding'))

