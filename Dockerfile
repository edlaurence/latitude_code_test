FROM python:3.7

# Create app directory
WORKDIR /app

# Install app dependencies
COPY src ./

RUN pip install -r requirements.txt

# Bundle app source
COPY src /app/src

# Run tests
CMD [ "python", "test_file_operator.py" ]
CMD [ "python", "problem_1.py" ]